

# The project is compiled and minimized

## Project use:

1. JQuery
2. SASS
3. Gulp
4. Webpack
5. apiv2.bitcoinaverage.com


## Before use webpack and gulp:
```sh
$ npm install
```
## For compile project use this command:
```sh
$ webpack
and
$ gulp watch
```