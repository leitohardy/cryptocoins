let sass = require('gulp-sass');
let gulp = require('gulp');
let cleanCSS = require('gulp-clean-css');
let autoprefixer = require('gulp-autoprefixer');
let rename = require('gulp-rename');

//style paths
let sassFiles = './scss/**/*.scss',
    cssDest = './public/css/';

gulp.task('default', function(){
    gulp.src(sassFiles)
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer())
        //.pipe(gulp.dest(cssDest))
        .pipe(cleanCSS())
        .pipe(rename({ extname: '.min.css' }))
        .pipe(gulp.dest(cssDest))
});

gulp.task('watch', function() {
    gulp.watch(sassFiles, ['default']); 
});