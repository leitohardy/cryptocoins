const path = require('path');

module.exports = {
    mode: 'production',
    context: path.resolve(__dirname, './'),
    entry: {
        app: './js/script.js'
    },

    output: {
        path: path.resolve(__dirname, './public/js'),
        filename: 'app.bundle.js',
        publicPath: '/public/js'
    },

    devServer: {
        contentBase: path.resolve(__dirname, './src')
    },

    module: {
        rules: [
            { test: /\.js$/, exclude: /node_modules/, loader: "babel-loader" }
        ]
    },

    //watch: true,
    
};

