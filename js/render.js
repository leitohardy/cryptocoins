function render(
    obj = {
        name : 'ethereum',
        price : '13',
        changes : {
            precent : {
                hour : 266,
                day : 266,
                week :  266,
                month : 266
            },
            price : {
                hour : 26,
                day : 67,
                week :  56,
                month : 34
            }
        }
    },
    currency = 'USD'
){

    let currencySymbol = '';


    if (currency == 'USD') {
        currencySymbol = '$';
        obj.price = currencySymbol + obj.price;
    
    } else if (currency == 'RUB') {
        currencySymbol = '₽';
        obj.price = '₽' + obj.price;

    } else if (currency == 'EUR') {
        currencySymbol = '€';
        obj.price = '€' + obj.price;

    } else if (currency == 'GBP') {
        currencySymbol = '£';
        obj.price = '£' + obj.price;

    }
    
    $(`.${obj.name} .coin-price .value`).html(obj.price);

    // hour
    if (obj.changes.precent.hour < 0) {
        $(`.${obj.name} .coin-date-changes .hourPercent`).parent().addClass('red');
        $(`.${obj.name} .coin-date-changes .hourPercent`).html(obj.changes.precent.hour+'%');
    } else {
        $(`.${obj.name} .coin-date-changes .hourPercent`).parent().removeClass('red');
        $(`.${obj.name} .coin-date-changes .hourPercent`).html('+'+obj.changes.precent.hour+'%');
    }

    if (obj.changes.price.hour < 0) {
        $(`.${obj.name} .coin-date-changes .hourPrice`).parent().addClass('red');
        $(`.${obj.name} .coin-date-changes .hourPrice`).html(obj.changes.price.hour+currencySymbol);
    } else {
        $(`.${obj.name} .coin-date-changes .hourPrice`).parent().removeClass('red');
        $(`.${obj.name} .coin-date-changes .hourPrice`).html('+'+obj.changes.price.hour+currencySymbol);
    }


    // DAYS
    if (obj.changes.precent.day < 0) {
        $(`.${obj.name} .coin-date-changes .dayPercent`).parent().addClass('red');
        $(`.${obj.name} .coin-date-changes .dayPercent`).html(obj.changes.precent.day+'%');
    } else {
        $(`.${obj.name} .coin-date-changes .dayPercent`).parent().removeClass('red');
        $(`.${obj.name} .coin-date-changes .dayPercent`).html('+'+obj.changes.precent.day+'%');
    }
    if (obj.changes.price.day < 0) {
        $(`.${obj.name} .coin-date-changes .dayPrice`).parent().addClass('red');
        $(`.${obj.name} .coin-date-changes .dayPrice`).html(obj.changes.price.day+currencySymbol);
    } else {
        $(`.${obj.name} .coin-date-changes .dayPrice`).parent().removeClass('red');
        $(`.${obj.name} .coin-date-changes .dayPrice`).html('+'+obj.changes.price.day+currencySymbol);
    }

    // week
    if (obj.changes.precent.week < 0) {
        $(`.${obj.name} .coin-date-changes .weekPercent`).parent().addClass('red');
        $(`.${obj.name} .coin-date-changes .weekPercent`).html(obj.changes.precent.week+'%');
    } else {
        $(`.${obj.name} .coin-date-changes .weekPercent`).parent().removeClass('red');
        $(`.${obj.name} .coin-date-changes .weekPercent`).html('+'+obj.changes.precent.week+'%');
    }

    if (obj.changes.price.week < 0) {
        $(`.${obj.name} .coin-date-changes .weekPrice`).parent().addClass('red');
        $(`.${obj.name} .coin-date-changes .weekPrice`).html(obj.changes.price.week+currencySymbol);
    } else {
        $(`.${obj.name} .coin-date-changes .weekPrice`).parent().removeClass('red');
        $(`.${obj.name} .coin-date-changes .weekPrice`).html('+'+obj.changes.price.week+currencySymbol);
    }

    // month
    if (obj.changes.precent.month < 0) {
        $(`.${obj.name} .coin-date-changes .monthPercent`).parent().addClass('red');
        $(`.${obj.name} .coin-date-changes .monthPercent`).html(obj.changes.precent.month+'%');
    } else {
        $(`.${obj.name} .coin-date-changes .monthPercent`).parent().removeClass('red');
        $(`.${obj.name} .coin-date-changes .monthPercent`).html('+'+obj.changes.precent.month+'%');
    }

    if (obj.changes.price.month < 0) {
        $(`.${obj.name} .coin-date-changes .monthPrice`).parent().addClass('red');
        $(`.${obj.name} .coin-date-changes .monthPrice`).html(obj.changes.price.month+currencySymbol);
    } else {
        $(`.${obj.name} .coin-date-changes .monthPrice`).parent().removeClass('red');
        $(`.${obj.name} .coin-date-changes .monthPrice`).html('+'+obj.changes.price.month+currencySymbol);
    }


}




export {render}