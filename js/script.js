//import {coins} from './staticData';
import {getData} from './getApiData';

$(document).ready(function(){

    let listBlock = $('.currency-list');
    let prevCurrency = 'USD';
    let currency = 'USD';

    getData(currency);

    // custom select logic

    $('.currency-select').click(function(){
        listBlock.toggle();
    });

    $('.currency-list li').click(function(){

        prevCurrency = $('.select-title').html();
        $('.select-title').html($(this).html());
        currency = $(this).html();
        $(this).html(prevCurrency);

        getData(currency);
    });
    // end select

    // switcher logic
    $('.switcher').click(function(){

        let coinName = $(this).data('coin-name');

        $(this).toggleClass('active');
        $(this).find('div.btn').toggleClass('active');

        $('.'+coinName+' .coin-date-changes .value .price').toggle();
        $('.'+coinName+' .coin-date-changes .value .percent').toggle();

    });
    // end switcher

});