import {render} from './render';

/*
Bitcoin (BTC), Bitcoin Cash (BCH), Litecoin (LTC), Ethereum (ETH), Dash (DASH), Ripple (XRP), Monero (XMR)
//ETHUSD
//LTCUSD
//BTCUSD
*/

//https://apiv2.bitcoinaverage.com/indices/global/ticker/BTCUSD
//https://apiv2.bitcoinaverage.com/indices/global/ticker/BTCEUR
//https://apiv2.bitcoinaverage.com/indices/global/ticker/BTCGBP

function getData(currency) {

    let promise = new Promise((resolve, rejects) => {

        $.ajax({
            url: "https://apiv2.bitcoinaverage.com/indices/global/ticker/ETH"+currency, 
            success: function(data){
                resolve(data);

                let objForRender = {
                    name : 'ethereum',
                    price : data.last,
                    changes : {
                        precent : {
                            hour : data.changes.percent.hour,
                            day : data.changes.percent.day,
                            week :  data.changes.percent.week,
                            month : data.changes.percent.month
                        },
                        price : {
                            hour : data.changes.price.hour,
                            day : data.changes.price.day,
                            week :  data.changes.price.week,
                            month : data.changes.price.month,
                        }
                    }
                }
                render(objForRender,currency);
                
            },
            error: function(err){
                rejects(err);
            }
        });
    });

    promise
        .then( (data) => {
            return data;
        })
        .then( (data) => {

            return new Promise((resolve, rejects) => {
                $.ajax({
                    url: "https://apiv2.bitcoinaverage.com/indices/global/ticker/LTC"+currency, 
                    success: function(data){
                        resolve(data);


                        let objForRender = {
                            name : 'litecoin',
                            price : data.last,
                            changes : {
                                precent : {
                                    hour : data.changes.percent.hour,
                                    day : data.changes.percent.day,
                                    week :  data.changes.percent.week,
                                    month : data.changes.percent.month
                                },
                                price : {
                                    hour : data.changes.price.hour,
                                    day : data.changes.price.day,
                                    week :  data.changes.price.week,
                                    month : data.changes.price.month,
                                }
                            }
                        }
                        render(objForRender,currency);

                    },
                    error: function(err){
                        rejects(err);
                    }
                });
                
            });

        })
        .then( (data) => {

            return new Promise((resolve, rejects) => {
                $.ajax({
                    url: "https://apiv2.bitcoinaverage.com/indices/global/ticker/BTC"+currency, 
                    success: function(data){
                        resolve(data);

                        let objForRender = {
                            name : 'bitcoin',
                            price : data.last,
                            changes : {
                                precent : {
                                    hour : data.changes.percent.hour,
                                    day : data.changes.percent.day,
                                    week :  data.changes.percent.week,
                                    month : data.changes.percent.month
                                },
                                price : {
                                    hour : data.changes.price.hour,
                                    day : data.changes.price.day,
                                    week :  data.changes.price.week,
                                    month : data.changes.price.month,
                                }
                            }
                        }
                        render(objForRender,currency);
                    },
                    error: function(err){
                        rejects(err);
                    }
                });
                
            });

        })
        .catch( (err) => {
            console.info(err);
        });

}

export {getData}