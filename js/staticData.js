

let coins = {
    ethereum : {
        name : 'Ethereum',
        data : {
            price : 22526.92,
            switcher : true,
            changes : {
                percent : {
                    hour : 266,
                    day : 266,
                    week : 266,
                    month : 266
                },
                price : {
                    hour : 266,
                    day : 266,
                    week : 266,
                    month : 266
                }
            }
        }
        
    },
    litecoin : {
        name : 'Litecoin',
        data : {
            price : 22526.92,
            switcher : true,
            changes : {
                percent : {
                    hour : 266,
                    day : 266,
                    week : 266,
                    month : 266
                },
                price : {
                    hour : 266,
                    day : 266,
                    week : 266,
                    month : 266
                }
            }
        }
    },
    bitcoin : {
        name : 'Bitcoin',
        data : {
            price : 22526.92,
            switcher : true,
            changes : {
                percent : {
                    hour : 266,
                    day : 266,
                    week : 266,
                    month : 266
                },
                price : {
                    hour : 266,
                    day : 266,
                    week : 266,
                    month : 266
                }
            }
        }
    }
}


export {coins};